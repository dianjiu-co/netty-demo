package co.dianjiu.bio;

import co.dianjiu.bio.server.BIOServerThreadReader;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.net.ServerSocket;
import java.net.Socket;


@SpringBootApplication
public class Application implements CommandLineRunner {

    @Value("${bio.port}")
    private int port;

    @Value("${bio.url}")
    private String url;

    public static void main(String[] args) {
        System.setProperty("cfg.env", "local");
        SpringApplication.run(Application.class, args);
    }

    @Override
    public void run(String... args) throws Exception {
        ServerSocket socket = new ServerSocket(port);
        System.out.println("服务端已经启动，监听端口为：" + port);
        boolean flag = true;
        while (flag) {
            Socket client = socket.accept();
            new BIOServerThreadReader(client).start();
        }
        socket.close();
    }
}
