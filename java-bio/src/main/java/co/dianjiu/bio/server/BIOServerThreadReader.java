package co.dianjiu.bio.server;

import java.io.IOException;
import java.io.PrintStream;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.Scanner;

/**
 * @author DianJiu
 * @website https://dianjiu.co
 * @email dianjiu@dianjiu.cc
 * @date 2021/8/2 17:53
 * @desc TODO
 */
public class BIOServerThreadReader extends Thread {
    private Socket client;
    private Scanner scanner;
    private PrintStream out;
    private boolean flag = true;

    public BIOServerThreadReader(Socket client) {
        this.client = client;
        try {
            this.scanner = new Scanner(this.client.getInputStream());
            this.scanner.useDelimiter("\n");
            this.out = new PrintStream(this.client.getOutputStream());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void run() {
        while (this.flag) {
            if (this.scanner.hasNext()) {
                String var = this.scanner.next().trim();
                System.out.println("收到客户端发来的" + var);
                if ("886".equals(var)) {
                    this.out.print("【BIOServer reply===>】" + "bye");
                    this.flag = false;
                } else {
                    out.println("【BIOServer reply===>】" + var);
                }
            }
        }
        try {
            this.scanner.close();
            this.out.close();
            this.client.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
