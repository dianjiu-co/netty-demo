## Netty Demo
Netty 学习笔记本和demo演示



## 开发环境

>   Zulu JDK 11
>
>   IDEA 2021.2
>
>   Netty 4.1.66.Final
>
>   Mina 2.1.4

## 学习步骤

| 学习步骤 | 项目名称        | 项目备注           | 端口 |
| -------- | --------------- | ------------------ | ---- |
| 1        | java-bio        | 传统的BIO编程      | 8010 |
| 2        | java-vio        | 伪异步的I/O编程    | 8020 |
| 3        | java-nio        | NIO编程            | 8030 |
| 4        | java-aio        | AIO编程            | 8040 |
| 5        | socket-server   | Socket服务端       | 8050 |
| 6        | socket-client   | Socket客户端       | 8060 |
| 7        | socket-chatroom | 基于Socket的聊天室 | 8070 |
| 8        | netty-server    | Netty服务端        | 8080 |
| 9        | netty-client    | Netty客户端        | 8090 |
| 10       | netty-chatroom  | 基于Netty的聊天室  | 9010 |
| 11       | mina-server     | Mina服务端         | 9020 |
| 12       | mina-client     | Mina客户端         | 9030 |
| 13       | mina-chatroom   | 基于Mina的聊天室   | 9040 |

