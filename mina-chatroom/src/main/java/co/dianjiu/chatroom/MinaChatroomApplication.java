package co.dianjiu.chatroom;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MinaChatroomApplication {

    public static void main(String[] args) {
        SpringApplication.run(MinaChatroomApplication.class, args);
    }

}
