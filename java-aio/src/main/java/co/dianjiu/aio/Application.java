package co.dianjiu.aio;

import co.dianjiu.aio.server.AIOServerHandler;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.net.InetSocketAddress;
import java.nio.channels.AsynchronousServerSocketChannel;
import java.nio.channels.Selector;

@SpringBootApplication
public class Application implements CommandLineRunner {
    private Selector selector ;

    @Value("${aio.port}")
    private int port;

    @Value("${aio.url}")
    private String url;

    public static void main(String[] args) {
        System.setProperty("cfg.env","local");
        SpringApplication.run(Application.class, args);
    }

    @Override
    public void run(String... args) throws Exception {
        AsynchronousServerSocketChannel server = AsynchronousServerSocketChannel.open()
                .bind(new InetSocketAddress(port));
        //Future 方式
        new AIOServerHandler(server).startWithFuture();
        //Callback 方式
        //new AIOServerHandler(server).startWithCompletionHandler();
    }
}
