package co.dianjiu.vio;

import co.dianjiu.vio.server.VIOServerHandler;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.net.ServerSocket;
import java.net.Socket;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

@SpringBootApplication
public class Application implements CommandLineRunner {

    @Value("${bio.port}")
    private int port;

    @Value("${bio.url}")
    private String url;

    public static void main(String[] args) {
        System.setProperty("cfg.env","local");
        SpringApplication.run(Application.class, args);
    }

    @Override
    public void run(String... args) throws Exception {
        ServerSocket socket = new ServerSocket(port);
        System.out.println("服务端已经启动，监听端口为：" + port);
        boolean flag = true;
        ExecutorService executorService = Executors.newFixedThreadPool(10);
        while (flag){
            Socket client = socket.accept();
            executorService.submit(new VIOServerHandler(client));
        }
        executorService.shutdown();
        socket.close();
    }
}
