package co.dianjiu.vio.client;

import co.dianjiu.vio.util.InputUtils;

import java.io.PrintStream;
import java.net.Socket;
import java.util.Scanner;

/**
 * @author DianJiu
 * @website https://dianjiu.co
 * @email dianjiu@dianjiu.cc
 * @date 2021/8/2 17:20
 * @desc TODO
 */
public class VIOClient {
    public static void main(String[] args) throws Exception{
        Socket client = new Socket("127.0.0.1", 10010);
        Scanner scan = new Scanner(client.getInputStream());
        scan.useDelimiter("\n");
        PrintStream out = new PrintStream(client.getOutputStream());
        boolean flag = true;
        while (flag){
            String inputData = InputUtils.getString("请输入要发送的内容：").trim();
            out.println(inputData);
            if (scan.hasNext()){
                String str = scan.next();
                System.out.println(str);
            }
            if ("886".equalsIgnoreCase(inputData)){
                flag = false;
            }
        }
        client.close();
    }
}
