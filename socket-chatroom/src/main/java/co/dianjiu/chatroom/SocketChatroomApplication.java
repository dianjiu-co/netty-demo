package co.dianjiu.chatroom;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SocketChatroomApplication {

    public static void main(String[] args) {
        SpringApplication.run(SocketChatroomApplication.class, args);
    }

}
